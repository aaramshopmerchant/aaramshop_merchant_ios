


#define kLoginSuccessfulNotificationName    @"LoginSuccessful"
#define kLogoutSuccessfulNotificationName  @"LogoutSuccessful"
#pragma mark - Alert titles and Internet alerts

#define kAlertTitle         @"Aaram shop"
#define kAlertBtnOK         @"OK"
#define kAlertBtnYES        @"YES"
#define kAlertBtnNO         @"NO"
#define kAlertCheckInternetConnection                                                         @"Please make sure that your device is connected to the internet"
#define kRequestTimeOutMessage                  @"We can't establish a connection to Aaram Shop servers. Please make sure that your device is connected to the internet and try again"

#define kAlertServiceFailed                         @"Failed. Try again" //new

#define kAlertValidEmail                            @"Please enter a valid email address"
#define kAlertPasswordLength                        @"Your password should have at least 6 digits"


#pragma mark - Common keys

#define APP_DELEGATE (AppDelegate*)[[UIApplication sharedApplication]delegate]

#define kBaseURL                              @"http://52.74.200.130:80/index.php/merchant/"

#define kIsLoggedIn                           @"isLoggedIn"
#define kDevice                               @"1"
#define kDeviceType                           @"deviceType"
#define kDeviceId                             @"deviceId"
#define kUsername                             @"username"
#define kpassword                             @"password"
#define kMessage                            @"message"
#define kLatitude                             @"latitude"
#define kLongitude                            @"longitude"
#define kSessionToken                         @"sessionToken"
#define kUserId                               @"userId"
#define kOption                               @"option"
#define kSessionToken                         @"sessionToken"
#define kMobile                               @"mobile"
#define kOptionLogin                          @"login"
#define kstatus                               @"status"
#define kOptionNew_user_login                 @"new_user_login"
#define kImage                                @"Img"
#define kPhoneNumbers                         @"phoneNumbers"
#define kFirstName                          @"firstName"
#define kLastName                           @"lastName"
#define kProfileImage                         @"profileImage"

#pragma mark - Existing User

#define kOptionStore_listing                @"store_listing"
#define kRadius                             @"radius"
#define kCity_name                          @"city_name"
#define kDelivers                           @"delivers"
#define kLocality_name                      @"locality_name"
#define kMinimum_order                      @"minimum_order"
#define kState_name                         @"state_name"
#define kStore_address                      @"store_address"
#define kStore_category                     @"store_category"
#define kStore_closing_days                 @"store_closing_days"
#define kStore_code                         @"store_code"
#define kStore_state                        @"store_state"
#define kStore_city                             @"store_city"
#define kStore_contact_person           @"store_contact_person"
#define kStore_distance                     @"store_distance"
#define kStore_email                        @"store_email"
#define kStore_id                           @"store_id"
#define kStore_image                        @"store_image"
#define kStore_latitude                     @"store_latitude"
#define kStore_longitude                    @"store_longitude"
#define kStore_mobile                       @"store_mobile"
#define kStore_name                         @"store_name"
#define kStore_person                       @"store_person"
#define kStore_phone                        @"store_phone"
#define kStore_pincode                      @"store_pincode"
#define kStore_terms                        @"store_terms"
#define kStore_working_from                 @"store_working_from"
#define kStore_working_status               @"store_working_status"
#define kStore_closing_days                  @"store_closing_days"
#define kStore_working_to                   @"store_working_to"
#define kRecommended_store              @"recommended_store"

#define kData                               @"data"

#pragma mark UpdateMobile

#define kOptionUpdate_user                    @"update_user"

#define kTextFieldDigitRange @"0123456789"


#pragma mark - New User Keys

#define kIsValid                @"isValid"

#pragma mark - OTP Validation Keys

#define kOptionUser_mobile_data           @"user_mobile_data"
//==================Sign Up============

#define kFileType                                   @"fileType"


/////////////////////////////////////////////
//Search The List of user ///////////////////

#define kSeachUrl @"search"
#define kFriendSearch @"friendSearch"
#define kItemCount @"itemCount"
#define kSearchCount 10
#define kPageNumber @"pageNumber"
#define kSearchString @"searchString"
#define kProfilePic @"profilePic"
//====================NetworkService.h=======================

#define NULLVALUE(m) (m == [NSNULL null]? @"":m)

#define SUCCESS                @"status"
#define DATA                    @"data"

#define KPost                 @"POST"
#define KGet                  @"GET"
#define KPut                  @"PUT"

#define kParameter @"Parameter"
#define kDataDic @"DataDic"
#define kBodyStr @"BodyStr"

//========= uploading Image =====
#define kMyUpload     @"myUpload"
#define kProfilePhoto @"profilePhoto"
#define kBoundry @"Boundry"
#define kServerUserNameId @"userNameId"


#pragma mark - 

#define kContactsAccessPermission           @"contactsAccessPermission"
#define kType                               @"type"

#pragma mark - 
#define kAddressForLocation                 @"addressForLocation"
#define kAddressTitle                       @"addressTitle"
#define kHomeAddress                        @"homeAddress"
#define kOfficeAddress                      @"officeAddress"
#define kAddressValue                       @"addressValue"


//=========Font Names=========
#define kRobotMedium            @"Roboto-Medium"
#define kRobotRegular              @"Roboto-Regular"
#define kRobotBold                  @"Roboto-Bold"
#define kMyriadProRegular       @"MyriadPro_Regular"
#define kMyriadProBold              @"MyriadPro_Bold"
#define kRocketDinner               @"RocketDiner_0"
#define kRocketDinnerCon        @"RocketDinerCondensed_0"

//========OffersViewController=======

//========================================================================
#pragma mark - Offers API key

#define kCurrentOfferURL               @"currentOffer"
#define kAaramshopId                        @"aaramshopId"
#define kOffers                                   @"offers"
#define kActivity_id                               @"activity_id"
#define kActivity_name                          @"activity_name"
#define kCompany_name                      @"company_name"
#define kBrand_name                          @"brand_name"
#define kActivity_details                      @"activity_details"
#define kBrandImage                           @"image"
#define kStart_date                              @"start_date"
#define kEnd_date                                @"end_date"
#define kRedemption                             @"redemption"
//========================================================================
#pragma mark - Verify Coupon Code API key

#define kVerifyCouponCodeURL               @"verifyCouponCode"
#define kActivityId                              @"activityId"
#define kCoupon_code                        @"coupon_code"
#define kMobile                                   @"mobile"
#define kName                                     @"name"

//========================================================================
#pragma mark - Offers Keys

#define kOffersURL  @"currentOffer"

//========================================================================
#pragma mark - Merchant Registration

#pragma mark - Mobile Register Keys

#define kMerchantRegistrationURL            @"merchantRegistration"

#define kCountryName                                @"countryName"
#define kMobile_verified                              @"mobile_verified"

//========================================================================
#pragma mark - OTP send Keys

#define kOtpValidateURL                @"otpValidate"
#define kOtp                                    @"otp"

//========================================================================
#pragma mark - OTP Resend keys
#define kResendOtpURL           @"resendOtp"

//========================================================================
#pragma mark - Merchant Detail API keys

#define kMerhantDetailsURL         @"merhantDetails"
#define kStoreName                         @"storeName"
#define kFullName                           @"fullName"
#define kEmailId                                @"emailId"

//========================================================================
#pragma mark - Merchant Address 

#define kMerchantAddressURL                @"merchantAddress"
#define kAddress                                        @"address"
#define kPincode                                        @"pincode"
#define kLocality                                        @"locality"
#define kCity                                              @"city"
#define kState                                             @"state"
#define kStoreCode                                    @"storeCode"
