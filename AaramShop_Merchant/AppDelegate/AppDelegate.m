//
//  AppDelegate.m
//  AaramShop_Merchant
//
//  Created by Pradeep Singh on 23/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
{
    Reachability *aReachability;
}
@end

@implementation AppDelegate

@synthesize navController,myCurrentLocation,locationManager,arrOptions;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
     [self initializeAllSingletonObjects];
    
    //Status bar color
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[UIColor blackColor]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    // view controllers selection to display
    if ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"3304645e047e061df52d0635ac8171941826e6dc467aff1d5e12d4c8d4da6be0" forKey:kDeviceId];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else
    {
        if(![[NSUserDefaults standardUserDefaults]valueForKey:kDeviceId])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"1234567890" forKey:kDeviceId];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
//    if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsLoggedIn] == YES) {
//        [[AppManager sharedManager] performSelector:@selector(fetchAddressBookWithContactModel) withObject:nil];
//        
//    }
    
    arrOptions = [NSArray arrayWithObjects:@"Home Address",@"Office Address", @"Others", nil];
    [self registerDeviceForDeviceToken:application];
    [self findCurrentLocation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessful:) name:kLoginSuccessfulNotificationName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:kLogoutSuccessfulNotificationName object:nil];

    if ([[NSUserDefaults standardUserDefaults] valueForKey:kStore_code] && [[[NSUserDefaults standardUserDefaults] valueForKey:kStore_code]length]>0)
    {
        [self loginSuccessful:nil];
    }else{
        [self logout:nil];
    }

    
    [self.window makeKeyAndVisible];
    
    return YES;
}
//-(void)findCurrentLocation
//{
//    locationManager=[[CLLocationManager alloc] init];
//    geocoder = [[CLGeocoder alloc] init];
//    
//    if ([CLLocationManager locationServicesEnabled])
//    {
//        locationManager.delegate = self;
//        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
//        locationManager.headingFilter = 1;
////
////        locationManager.distanceFilter = kCLDistanceFilterNone;//5; // Don't use other value than - 'kCLDistanceFilterNone', else the local notification will not work.
//        
//        
//        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
//        {
//            [locationManager requestWhenInUseAuthorization];
//        }
//        [locationManager startUpdatingLocation];
//        
//        
//    }
//}
-(void)findCurrentLocation
{
    
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager.delegate = self;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];

    }
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager* )manager didFailWithError:(NSError *)error
{
    
}


- (void)locationManager:(CLLocationManager* )manager didUpdateLocations:(NSArray* )locations
{
    CLLocation* newLocation = [locations lastObject];
    
    [self getUpdatedLocation:newLocation];
}


-(void)getUpdatedLocation:(CLLocation *)newLocation
{
    myCurrentLocation = newLocation;
}



- (void)applicationWillResignActive:(UIApplication *)application {

    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    if([Utils isInternetAvailable])
    {
        [self sendPresence:@"away"];
    }
    [self performSelector:@selector(disconnectXmpp) withObject:nil afterDelay:300];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STOPAUDIO" object:nil];
    //7-3-14
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    NSMutableArray *aPasteArr =  [NSMutableArray arrayWithArray: [pb strings]];
    if ([aPasteArr containsObject:kMESSAGEFORWARD])
    {
        [pb setStrings: Nil];
    }
    //end
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"*****beginBackgroundTaskWithExpirationHandler");
        
        [gCXMPPController disconnect];
        //        XMPPPresence *presence = [XMPPPresence presence];
        //        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        //        [status setStringValue:@"busy"];
        //        [presence addChild:status];
        //        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        //        [gCXMPPController.xmppStream sendElement:presence];
        [app endBackgroundTask:bgTask];
    }];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}
- (void)disconnectXmpp
{
    if([[UIApplication sharedApplication] applicationState]==UIApplicationStateBackground)
    {
        NSLog(@"***** Disconnect xmpp *****");
        
        //        [gCXMPPController disconnect];
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        [status setStringValue:@"busy"];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
        
    }
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if([Utils isInternetAvailable])
    {
        [self sendPresence:@"online"];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    if([Utils isInternetAvailable])
    {
        //        [gCXMPPController disconnect];
        
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        [status setStringValue:@"busy"];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Approutes.AaramShop_Merchant" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AaramShop_Merchant" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AaramShop_Merchant.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Register Device For Device Token
-(void)registerDeviceForDeviceToken:(UIApplication *)application
{
    if ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"3304645e047e061df52d0635ac8171941826e6dc467aff1d5e12d4c8d4da6be0" forKey:kDeviceId];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
    {
        if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
#ifdef __IPHONE_8_0
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                                 |UIRemoteNotificationTypeSound
                                                                                                 |UIRemoteNotificationTypeAlert) categories:nil];
            [application registerUserNotificationSettings:settings];
#endif
        }
        else {
            UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
            [application registerForRemoteNotificationTypes:myTypes];
        }
    }
}

#pragma mark - tabbar methods

- (void) loginSuccessful:(NSNotification *) aNotification{
//    [self loginUpdateScreen];
//}
//
//- (void) loginUpdateScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.tabBarController=[storyboard instantiateViewControllerWithIdentifier:@"tabbarScreen"];
    [self setTabBarImages];
//    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:254.0/255.0f green:56.0/255.0f blue:45.0/255.0f alpha:1.0f];
    [gCXMPPController connect];
    self.tabBarController.delegate = self;
    [self.window setRootViewController:self.tabBarController];
    [self.tabBarController setSelectedIndex:3];
    self.navController = nil;
}

- (void) logout:(NSNotification *) aNotification{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.navController = [storyboard instantiateViewControllerWithIdentifier:@"optionNav"];
    [self.window setRootViewController:self.navController];
    self.tabBarController = nil;
    //    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
}
- (void) setTabBarImages{
    for(int i = 0 ; i < 5 ; i ++){
        UITabBarItem *tabBarItem = [self.tabBarController.tabBar.items objectAtIndex:i];
        UIImage *image;
        UIImage *image_sel;
        
        switch (i) {
            case 0:
                
                image = [UIImage imageNamed:@"tabBarHomeIcon"];
                image_sel = [UIImage imageNamed:@"tabBarHomeIconActive"];
                tabBarItem.title = @"Home";
                break;
            case 1:
                
                image = [UIImage imageNamed:@"tabBarChatIcon"];
                image_sel = [UIImage imageNamed:@"tabBarChatIconActive"];
                tabBarItem.title = @"Chat";
                
                break;
            case 2:
                
                image = [UIImage imageNamed:@"tabBarOrdersIcon"];
                image_sel = [UIImage imageNamed:@"tabBarOrdersIconActive"];
                tabBarItem.title = @"Orders";
                
                break;
            case 3:
                
                image = [UIImage imageNamed:@"tabBarOffersIcon"];
                image_sel = [UIImage imageNamed:@"tabBarOffersIconActive"];
                tabBarItem.title = @"Offers";
                
                break;
            case 4:
                
                image = [UIImage imageNamed:@"tabBarReportsIconInactive"];
                image_sel = [UIImage imageNamed:@"tabBarReportsIconActive"];
                tabBarItem.title = @"Reports";
                
                break;
            default:
                break;
        }
        if([image respondsToSelector:@selector(imageWithRenderingMode:)]){
            image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            image_sel = [image_sel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        [tabBarItem setImage:image];
        [tabBarItem setSelectedImage:image_sel];
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:254.0/255.0f green:56.0/255.0f blue:45.0/255.0f alpha:1.0f]];
        [tabBarItem imageInsets];
    }
}

#pragma mark - Chatting Methods

- (XMPPStream *)xmppStream {
    return [gCXMPPController xmppStream];
}
-(void)initializeAllSingletonObjects
{
    [CXMPPController sharedXMPPController];
    [AppManager sharedManager];
}

-(SMChatViewController *)createChatViewByChatUserNameIfNeeded:(NSString *)inChatUser
{
        UINavigationController *navcon = (UINavigationController*)self.tabBarController.selectedViewController;
    
        if (self.AllChatViewConDic == nil)
        {
            self.AllChatViewConDic = [[NSMutableDictionary alloc] init];
    
        }
        SMChatViewController *aChatViewCon = [self.AllChatViewConDic objectForKey: inChatUser];
        NSUInteger index=[[navcon childViewControllers] indexOfObject:aChatViewCon];
    
    
        if (aChatViewCon)
        {
            if (index == NSNotFound)
            {
                aChatViewCon.isAlreadyInStack= NO;
                aChatViewCon.eSMChatStatus = SMCHAT_NOT_IN_STACK;
    
            }
            else if (index == [[navcon childViewControllers] count]-1)
            {
                aChatViewCon.isAlreadyInStack= YES;
                aChatViewCon.eSMChatStatus = SMCHAT_AT_TOP_OF_STACK;
    
            }
            else
            {
                aChatViewCon.isAlreadyInStack= YES;
                aChatViewCon.eSMChatStatus = SMCHAT_EXIST_BUT_NOT_ON_TOP;
    
            }
        }
        else
        {
    
            aChatViewCon = [[SMChatViewController alloc] initWithNibName:@"SMChatViewController" bundle:nil];
            [self.AllChatViewConDic setObject: aChatViewCon forKey: inChatUser];
    
            aChatViewCon.isAlreadyInStack= NO;
            aChatViewCon.eSMChatStatus=SMCHAT_NOT_IN_STACK;
        }
        return  aChatViewCon;
    return nil;
}

-(SMChatViewController *)getChatViewDelegateByChatUserName:(NSString *)inChatUser
{
    SMChatViewController *aChatViewCon = [self.AllChatViewConDic objectForKey: inChatUser];
    return  aChatViewCon;
}

-(void)releaseChatViewByChatUserNameIfNeeded:(NSString *)inChatUser
{
    id aChatViewCon = [self.AllChatViewConDic objectForKey: inChatUser];
    
    if (aChatViewCon)
    {
        [self.AllChatViewConDic removeObjectForKey: inChatUser];
        
    }
}
-(void)sendPresence:(NSString *)type
{
    if([type isEqualToString:@"away"])
    {
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *show = [NSXMLElement elementWithName:@"show" stringValue:@"away"];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status" stringValue:@"away"];
        [presence addChild:show];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
    }
    else
    {
        XMPPPresence *presence = [XMPPPresence presence];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        [status setStringValue:@"online"];
        [presence addChild:status];
        [presence addAttributeWithName:@"date" stringValue:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [gCXMPPController.xmppStream sendElement:presence];
    }
}


//-(BOOL)openChatViewfromNotificationViewByFriendDetail:(FriendsDetails *)inFrndDetail
//{
//
//}
-(void)setChatWindowOpenedStatusBySender:(NSString*)inSender andBool:(BOOL)inBool
{
    if(inBool)
    {
        [[NSUserDefaults standardUserDefaults] setValue:inSender forKey:kMessageChatWithUser];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kMessageChatWithUser];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}
-(BOOL)getChatWindowOpenedStatusBySender:(NSString*)inSender
{
    if(inSender==nil)
        return NO;
    
    BOOL aStatus = NO;
    if([[[NSUserDefaults standardUserDefaults] valueForKey:kMessageChatWithUser]isEqualToString:inSender])
    {
        aStatus = YES;
    }
    return  aStatus;
}
-(BOOL)openChatViewfromNotificationViewByFriendDetailAnonymous:(NSString *)inFrndDetail
{
    //    UINavigationController *navcon = (UINavigationController*)self.tabBarController.selectedViewController;
    //    if([self getChatWindowOpenedStatusBySender:inFrndDetail])
    //    {
    //        if([navcon.topViewController isKindOfClass:[SMChatViewController class]])
    //            return NO;
    //    }
    //    SMChatViewController *chatView = nil;
    //    chatView = [self createChatViewByChatUserNameIfNeeded: inFrndDetail];
    //    chatView.chatWithUser =[[[inFrndDetail lowercaseString] stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
    //    chatView.userName = inFrndDetail;
    //    chatView.imageString = nil;
    //
    //    chatView.friendNameId = nil;
    //
    //    switch (chatView.eSMChatStatus) {
    //        case SMCHAT_AT_TOP_OF_STACK:
    //            return NO;
    //            break;
    //        case SMCHAT_EXIST_BUT_NOT_ON_TOP:
    //            [navcon popToViewController:chatView animated:YES];
    //            return YES;
    //            break;
    //        case SMCHAT_NOT_IN_STACK:
    //            [navcon pushViewController:chatView animated:YES];
    //            return YES;
    //            break;
    //        default:
    //            break;
    //    }
    return NO;
    
}
//-(BOOL)openChatViewfromNotificationViewByFriendDetail:(AddressBookDB *)inFrndDetail
//{
//    UINavigationController *navcon = (UINavigationController*)self.tabBarController.selectedViewController;
//    if([self getChatWindowOpenedStatusBySender:inFrndDetail.orgChatUsername])
//    {
//        if([navcon.topViewController isKindOfClass:[SMChatViewController class]])
//            return NO;
//    }
//    SMChatViewController *chatView = nil;
//    chatView = [self createChatViewByChatUserNameIfNeeded: [inFrndDetail.orgChatUsername lowercaseString]];
//    chatView.chatWithUser =[[[inFrndDetail.orgChatUsername lowercaseString] stringByAppendingString:[NSString stringWithFormat:@"@%@",STRChatServerURL]]lowercaseString];
//    chatView.userName = inFrndDetail.fullName;
//    chatView.imageString = inFrndDetail.profilePic;
//
//    chatView.friendNameId = [NSString stringWithFormat:@"%@",inFrndDetail.userId];
//
//    switch (chatView.eSMChatStatus) {
//        case SMCHAT_AT_TOP_OF_STACK:
//            return NO;
//            break;
//        case SMCHAT_EXIST_BUT_NOT_ON_TOP:
//            [navcon popToViewController:chatView animated:YES];
//            return YES;
//            break;
//        case SMCHAT_NOT_IN_STACK:
//            [navcon pushViewController:chatView animated:YES];
//            return YES;
//            break;
//        default:
//            break;
//    }
//
//
//}
#pragma mark check internet avalability method

-(void)internetConnectionListener:(NSNotification *)inNotification
{
    if([Utils isInternetAvailable])
    {
        
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
        {
            if([[aReachability currentReachabilityString]isEqualToString:@"Cellular"])
            {
                [gCXMPPController disconnect];
            }
            if(![gCXMPPController isConnected])
            {
                
                [gCXMPPController connect];
            }
            [self sendPresence:@"away"];
        }
        else
        {
            [gCXMPPController disconnect];
            [gCXMPPController connect];
            [self sendPresence:@"online"];
            
        }
        
    }
    else
    {
    }
}

@end
