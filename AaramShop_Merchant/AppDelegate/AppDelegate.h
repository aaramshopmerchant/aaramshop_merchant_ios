//
//  AppDelegate.h
//  AaramShop_Merchant
//
//  Created by Pradeep Singh on 23/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SMChatViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,UITabBarControllerDelegate,UINavigationControllerDelegate>
{
    CLGeocoder *geocoder;
    UIBackgroundTaskIdentifier bgTask;
    __weak NSObject <SMMessageDelegate> *_messageDelegate;
}

// Core Data
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UINavigationController *navController;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (nonatomic, strong) CLLocation *myCurrentLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic,  strong) NSArray *arrOptions;


#pragma mark - chat
@property(nonatomic,assign)BOOL isChatViewOpened;
@property (strong, nonatomic) NSMutableDictionary *AllChatViewConDic;
@property (nonatomic, strong) SMChatViewController *chatViewController;

-(void)setChatWindowOpenedStatusBySender:(NSString*)inSender andBool:(BOOL)inBool;

-(BOOL)getChatWindowOpenedStatusBySender:(NSString*)inSender;
-(SMChatViewController *)createChatViewByChatUserNameIfNeeded:(NSString *)inChatUser;

-(void)releaseChatViewByChatUserNameIfNeeded:(NSString *)inChatUser;

-(SMChatViewController *)getChatViewDelegateByChatUserName:(NSString *)inChatUser;
-(void)sendPresence:(NSString *)type;
//-(BOOL)openChatViewfromNotificationViewByFriendDetail:(AddressBookDB *)inFrndDetail;
-(BOOL)openChatViewfromNotificationViewByFriendDetailAnonymous:(NSString *)inFrndDetail;

@end

