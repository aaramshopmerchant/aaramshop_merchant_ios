//
//  CustomAlertViewController.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 27/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "CustomAlertViewController.h"

@interface CustomAlertViewController ()

@end

@implementation CustomAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    txtViewMessage.text = self.message;
    // Do any additional setup after loading the view.
    if (self.isValid)
    {
        [Utils playSound:@"valid" ];
        lblALert.text = @"Congratulations";
        lblTitle.text = @"The coupon code is valid!!";
        lblTitle.textColor = [UIColor colorWithRed:36/255.0f green:138/255.0f blue:0/255.0f alpha:1.0f];
        [actionBtn setTitle:kAlertBtnOK forState:UIControlStateNormal];
    }
    else
    {
        [Utils playSound:@"notValid"];
        lblALert.text = @"Alert";
        lblTitle.text = @"The coupon code is NOT valid!!";
        lblTitle.textColor = [UIColor colorWithRed:189/255.0f green:0/255.0f blue:20/255.0f alpha:1.0f];
        [actionBtn setTitle:@"Try Again" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnAction:(id)sender {
    if (self.delegate && [self.delegate conformsToProtocol:@protocol(CustomAlertViewControllerDelegate)] && [self.delegate respondsToSelector:@selector(enableSubmitBtn)])
    {
        [self.delegate enableSubmitBtn];
    }
    [self.view removeFromSuperview];
}

@end
