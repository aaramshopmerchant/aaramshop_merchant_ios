//
//  CustomAlertViewController.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 27/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomAlertViewControllerDelegate <NSObject>

-(void)enableSubmitBtn;
@end

@interface CustomAlertViewController : UIViewController
{
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblALert;
    IBOutlet UITextView *txtViewMessage;
    __weak IBOutlet UIButton *actionBtn;
}
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *strAlert;
@property(nonatomic,weak) id<CustomAlertViewControllerDelegate> delegate;
@property (nonatomic, assign) BOOL isValid;
- (IBAction)btnAction:(id)sender;
@end
