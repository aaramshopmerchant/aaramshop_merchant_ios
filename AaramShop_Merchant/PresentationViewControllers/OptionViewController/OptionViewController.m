//
//  OptionViewController.m
//  AaramShop
//
//  Created by Approutes on 30/04/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import "OptionViewController.h"
#import "LoginViewController.h"
#import "MobileEnterViewController.h"
@interface OptionViewController ()

@end

@implementation OptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:kIsLoggedIn]) {
//        UITabBarController *tabBarController = (UITabBarController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"tabbarScreen"];
//        [self.navigationController pushViewController:tabBarController animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessfulNotificationName object:self userInfo:nil];


    }
}

- (IBAction)btnNewUserClick:(UIButton *)sender {
//    [[NSUserDefaults standardUserDefaults]  setValue:@"14326271637773@devchatmcare.approutes.com" forKey:kXMPPmyJID1];
//    [[NSUserDefaults standardUserDefaults]  setValue:@"5aee917004c013880a1ca89d69c1808bd7f1bcf8" forKey:kXMPPmyPassword1];
//    [[NSUserDefaults standardUserDefaults]  synchronize];
//    [gCXMPPController connect];
    MobileEnterViewController *mobileEnterVwController = (MobileEnterViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MobileEnterScreen" ];
    [self.navigationController pushViewController:mobileEnterVwController animated:YES];

}

- (IBAction)btnExistingUserClick:(UIButton *)sender {
    
//    [[NSUserDefaults standardUserDefaults]  setValue:@"14323611789915@devchatmcare.approutes.com" forKey:kXMPPmyJID1];
//    [[NSUserDefaults standardUserDefaults]  setValue:@"5aee917004c013880a1ca89d69c1808bd7f1bcf8" forKey:kXMPPmyPassword1];
//    [[NSUserDefaults standardUserDefaults]  synchronize];
//    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessfulNotificationName object:self userInfo:nil];

//    [gCXMPPController connect];
//
    LoginViewController *loginVwController = (LoginViewController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginScreen"];
    [self.navigationController pushViewController:loginVwController animated:YES];
}

#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
