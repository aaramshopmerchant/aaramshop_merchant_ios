//
//  SignupThirdViewController.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "SignupThirdViewController.h"

@interface SignupThirdViewController ()
{
    NSData *imageData;
}
@end

@implementation SignupThirdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self setUpNavigationBar];
    imageData = [[NSUserDefaults standardUserDefaults] objectForKey:kImage];
    if (imageData) {
        UIImage* image = [UIImage imageWithData:imageData];
        effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:image];
        imgBackground.image = effectImage;
        
    }
    else
    {
        imgBackground.image = [UIImage imageNamed:@"bGroundThree.jpg"];
    }
    
    arrStoreTimings = [[NSMutableArray alloc] init];
    arrAddLocality = [[NSMutableArray alloc] init];
    dataDict=[[NSMutableDictionary alloc]init];
    allSections =[[NSMutableArray alloc]init];
    [arrStoreTimings addObject: [NSDictionary dictionaryWithObjectsAndKeys:
                         @"Name",@"Key",
                         @"Rose Jackson",@"Value",nil]];
    [arrStoreTimings addObject: [NSDictionary dictionaryWithObjectsAndKeys:
                             @"Email Id",@"Key",
                             @"rosejackson@gmail.com",@"Value",nil]];
    [arrStoreTimings addObject: [NSDictionary dictionaryWithObjectsAndKeys:
                             @"Phone No.",@"Key",
                             @"9996866907",@"Value",nil]];
    
    [allSections addObject:@"StoreTimings"];
    [allSections addObject:@"AddLocality"];
    [dataDict setObject:arrStoreTimings forKey:@"StoreTimings"];
    [dataDict setObject:arrAddLocality forKey:@"AddLocality"];
    UITapGestureRecognizer *gst = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    gst.cancelsTouchesInView = NO;
    gst.delegate = self;
    [self.view addGestureRecognizer:gst];
}
-(void)setUpNavigationBar
{
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(0,0, 150, 44);
    UILabel* titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont fontWithName:@"Roboto_Regular" size:15];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.text = @"Additional Details";
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    UIImage *imgBack = [UIImage imageNamed:@"backBtn"];
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.bounds = CGRectMake( -10, 0, 30, 30);
    
    [backBtn setImage:imgBack forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barBtnBack = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    
    
    NSArray *arrBtnsLeft = [[NSArray alloc]initWithObjects:barBtnBack, nil];
    self.navigationItem.leftBarButtonItems = arrBtnsLeft;
    
    
    UIViewController *vCon ;
    
    [self.view addSubview: vCon.view];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)backBtn
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSignup:(id)sender {
//    UITabBarController *tabBarController = (UITabBarController *)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"tabbarScreen"];
//    [self.navigationController pushViewController:tabBarController animated:YES];
    

}

#pragma mark - keyboard Methods
-(void)hideKeyboard
{
//    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//    [txtEmail resignFirstResponder];
//    [txtFullName resignFirstResponder];
//    [txtPassword resignFirstResponder];
//    [txtStoreName resignFirstResponder];
}



#pragma mark - Table DataSource & Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    NSInteger toReturn = 1;
    
    if (allSections && allSections.count > 0)
    {
        toReturn = allSections.count;
    }
    return toReturn;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger toReturn = 0;
    NSArray *sectionArr = [dataDict objectForKey: [allSections  objectAtIndex: section]];
    
    if (sectionArr && sectionArr.count > 0)
    {
        toReturn = sectionArr.count;
    }
    return toReturn;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{UITableViewCell *tableCell = nil;
    
    switch (indexPath.section)
    {
        case esStoreTimimg:
        {
            switch (indexPath.row) {
                case erOpenClose:
                {
                    
                }
                    break;
                    case erHomeDelivery:
                {
                    
                }
                    break;
                    case erDeliveryRange:
                {
                    
                }
                    break;
                default:
                    break;
            }
        }
        default: break;
    }
//
//    static NSString *cellIdentifier = @"Cell";

//    ProfileTableCell *cell = (ProfileTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil) {
//        cell = [[ProfileTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
//    
//    NSDictionary *dataDic = [[dataDict objectForKey: [allSections objectAtIndex: indexPath.section]] objectAtIndex: indexPath.row];
//    
//    cell.indexPath=indexPath;
//    [cell updateCellWithData: dataDic];
    
    return tableCell;
}
//-(SelectionTableCell*)createCellTextField:(NSString*)cellIdentifier{
//    SelectionTableCell *cell = (SelectionTableCell *)[tblView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil) {
//        cell = [[SelectionTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
//    
//    return cell;
//    
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end
