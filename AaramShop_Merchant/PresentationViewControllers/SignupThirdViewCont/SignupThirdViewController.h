//
//  SignupThirdViewController.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TxtOpenCloseTableCell.h"
#import "SwtTableCell.h"
#import "SelectionTableCell.h"
typedef enum
{
    esStoreTimimg = 0,
    esAddLocality
}eSectionType;
typedef enum
{
    erOpenClose = 0,
    erHomeDelivery,
    erDeliveryRange
}eRowType;
@interface SignupThirdViewController : UIViewController<UIGestureRecognizerDelegate>
{
    __weak IBOutlet UIImageView *imgBackground;
    __weak IBOutlet UITableView *tblView;
    UIImage * effectImage;
    NSMutableArray *arrStoreTimings;
    NSMutableArray *arrAddLocality;
    NSMutableArray *allSections;
    NSMutableDictionary *dataDict;
}
- (IBAction)btnSignup:(id)sender;

@end
