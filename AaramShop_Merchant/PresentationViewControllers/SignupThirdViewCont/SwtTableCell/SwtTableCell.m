//
//  SwtTableCell.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 27/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "SwtTableCell.h"

@implementation SwtTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
