//
//  OffersTableCell.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 26/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "OffersTableCell.h"

@implementation OffersTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    
    if (self) {
        
        subView = [[UIView alloc]initWithFrame:CGRectZero];
        
        imgBrand = [[UIImageView alloc]initWithFrame:CGRectZero];
        imgBrand.contentMode = UIViewContentModeScaleAspectFit;
        
        
        lblBrandName = [[UILabel alloc]initWithFrame:CGRectZero];
        lblBrandName.font = [UIFont fontWithName:kRobotMedium size:15.0f];
        lblBrandName.textColor = [UIColor colorWithRed:44/255.0f green:44/255.0f blue:44/255.0f alpha:1.0f];
        lblBrandName.lineBreakMode = NSLineBreakByWordWrapping;
        lblBrandName.numberOfLines = 2;
//        lblBrandName.backgroundColor = [UIColor blackColor];
        
        
        lblRedemption = [[UIButton alloc]initWithFrame:CGRectZero];
        lblRedemption.titleLabel.font = [UIFont fontWithName:kRobotBold size:10.0f];
        [lblRedemption setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
//        lblRedemption.titleLabel.textAlignment = NSTextAlignmentRight;
        [lblRedemption setBackgroundImage:[UIImage imageNamed:@"tagBox"] forState:UIControlStateNormal];
        lblRedemption.titleEdgeInsets =  UIEdgeInsetsMake(-2, 0, 0, 0);

//        lblRedemption.backgroundColor = [UIColor blueColor];
        
        lblActivityName = [[UILabel alloc]initWithFrame:CGRectZero];
        lblActivityName.font = [UIFont fontWithName:kRobotRegular size:13.0f];
        lblActivityName.textColor = [UIColor colorWithRed:108/255.0f green:108/255.0f blue:108/255.0f alpha:1.0f];
//        lblActivityName.backgroundColor = [UIColor orangeColor];
        
        lblActivityDetail = [[UILabel alloc]initWithFrame:CGRectZero];
        lblActivityDetail.font = [UIFont fontWithName:kRobotRegular size:13.0f];
        lblActivityDetail.textColor = [UIColor colorWithRed:108/255.0f green:108/255.0f blue:108/255.0f alpha:1.0f];
//        lblActivityDetail.backgroundColor = [UIColor purpleColor];
        
        lblValidTill = [[UILabel alloc]initWithFrame:CGRectZero];
        lblValidTill.font = [UIFont fontWithName:kRobotRegular size:13.0f];
        lblValidTill.textColor = [UIColor colorWithRed:108/255.0f green:108/255.0f blue:108/255.0f alpha:1.0f];
//        lblValidTill.backgroundColor = [UIColor redColor];
        
        [self addSubview:subView];
        [subView addSubview:imgBrand];
        [self addSubview:lblBrandName];
        [self addSubview:lblRedemption];
        [self addSubview:lblActivityName];
//        [self addSubview:lblActivityDetail];
        [self addSubview:lblValidTill];
       
    }
    return self;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    // adjust frame here if needed.
    float imgWidth                       =   75;
    float imgHeight                     =  75;
    float lblheight                     =   16;
    float padding                       =   10;
    float lblWidthRedemption    = 80;
    
    CGRect subViewRect                     =   CGRectZero;
    CGRect imgBrandRect                     =   CGRectZero;
    CGRect lblBrandNameRect              =   CGRectZero;
    CGRect lblActivityNameRect           =   CGRectZero;
//    CGRect lblActivityDetailRect            =   CGRectZero;
    CGRect lblValidTillRect                   =   CGRectZero;
    CGRect lblRedemptionRect             =   CGRectZero;
    CGRect selfRect                              =   self.frame;
    
    subViewRect.size.width                 =   imgWidth;
    subViewRect.size.height                =   imgHeight;
    subViewRect.origin.x                     =   1;
    subViewRect.origin.y                     =   1;
    subView.frame                              =   subViewRect;
    
    imgBrandRect.size.width                 =   imgWidth;
    imgBrandRect.size.height                =   imgHeight;
    imgBrandRect.origin.x                     =   8;
    imgBrandRect.origin.y                     =   (selfRect.size.height - imgBrandRect.size.height)/2;
    imgBrand.frame                              =   imgBrandRect;
    
    
    
    lblBrandNameRect.size.width                 =   selfRect.size.width - lblWidthRedemption - imgBrandRect.size.width - padding-1;
    lblBrandNameRect.size.height                =   lblheight*2;
    lblBrandNameRect.origin.x                     =   imgBrandRect.origin.x + imgBrandRect.size.width + padding+1;
    lblBrandNameRect.origin.y                     =   4;
    lblBrandName.frame                              =   lblBrandNameRect;
    
    lblRedemptionRect.size.width                 =   lblWidthRedemption;
    lblRedemptionRect.size.height                =   25;
    lblRedemptionRect.origin.x                     =   self.frame.size.width -lblWidthRedemption +5;
    lblRedemptionRect.origin.y                     =   6;
    lblRedemption.frame                              =   lblRedemptionRect;
    
    lblActivityNameRect.size.width                 =   selfRect.size.width - padding*3 - imgBrandRect.size.width -1;
    lblActivityNameRect.size.height                =   lblheight;
    lblActivityNameRect.origin.x                     =   imgBrandRect.origin.x + imgBrandRect.size.width + padding+1;
    lblActivityNameRect.origin.y                     =   lblBrandNameRect.size.height + lblBrandNameRect.origin.y + 2;
    lblActivityName.frame                              =   lblActivityNameRect;

//    lblActivityDetailRect.size.width                 =   selfRect.size.width - padding*3 - imgBrandRect.size.width;
//    lblActivityDetailRect.size.height                =   lblheight;
//    lblActivityDetailRect.origin.x                     =   imgBrandRect.origin.x + imgBrandRect.size.width + padding;
//    lblActivityDetailRect.origin.y                     =   lblActivityNameRect.size.height + lblActivityNameRect.origin.y + 2;
//    lblActivityDetail.frame                              =   lblActivityDetailRect;
////
    
    lblValidTillRect.size.width                 =   selfRect.size.width - padding*3 - imgBrandRect.size.width - 1;
    lblValidTillRect.size.height                =   lblheight;
    lblValidTillRect.origin.x                     =   imgBrandRect.origin.x + imgBrandRect.size.width + padding + 1;
    lblValidTillRect.origin.y                     =   lblActivityNameRect.size.height + lblActivityNameRect.origin.y + 4;
    lblValidTill.frame                              =   lblValidTillRect;
//
//    

}
-(void)updateCellWithData:(Offers  *)offer
{
    [imgBrand setImageWithURL:[NSURL URLWithString:offer.image] placeholderImage:[UIImage imageNamed:@"defaultBrandImage"]];
    lblBrandName.text = offer.brand_name;
    lblActivityName.text = offer.activity_name;
//    lblActivityDetail.text = offer.activity_details;
    [lblRedemption setTitle:offer.redemption forState:UIControlStateNormal];
    lblValidTill.text = offer.end_date;
}
@end
