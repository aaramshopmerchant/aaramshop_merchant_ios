//
//  OffersViewController.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersViewController : UIViewController<AaramShop_ConnectionManager_Delegate>
{
    
    __weak IBOutlet UITableView *tblView;
}
@end
