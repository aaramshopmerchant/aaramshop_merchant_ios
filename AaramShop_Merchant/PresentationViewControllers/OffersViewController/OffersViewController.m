//
//  OffersViewController.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "OffersViewController.h"
#import "OffersTableCell.h"
#import "BrandRedemViewController.h"

@interface OffersViewController ()
{
    NSMutableArray * dataSource;
    AaramShop_ConnectionManager *aaramShop_ConnectionManager;
}
@end

@implementation OffersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpNavigationBar];
    dataSource = [[NSMutableArray alloc] init];
    aaramShop_ConnectionManager = [[AaramShop_ConnectionManager alloc] init];
    aaramShop_ConnectionManager.delegate = self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getOffers];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpNavigationBar
{
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(0,0, 150, 44);
    UILabel* titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont fontWithName:kRobotRegular size:15];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.text = @"Offers";
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    self.navigationItem.titleView = _headerTitleSubtitleView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table Delegates and DataSources
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 90;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 54;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *secView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, 54)];
    secView.backgroundColor = [UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1.0f];
    UIButton * btnHeader = [[UIButton alloc] initWithFrame:CGRectMake(7.5f, (secView.frame.size.height - 40)/2, [[UIScreen mainScreen]bounds].size.width - 15, 40)];
//    btnHeader.image =;
//    btnHeader setBackgroundColor:<#(UIColor *)#>];
    [btnHeader setBackgroundImage: [UIImage imageNamed:@"brandRedemptionSchemeBox"] forState:UIControlStateNormal];
    [btnHeader setTitle:@"Brand Redemption Scheme" forState:UIControlStateNormal];
    btnHeader.titleLabel.font = [UIFont fontWithName:kRobotRegular size:14.0f];
//    [btnHeader setTitleColor:];
    [btnHeader setTitleColor:[UIColor colorWithRed:44/255.0f green:44/255.0f blue:44/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [secView addSubview:btnHeader];
    return secView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    OffersTableCell *cell = (OffersTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[OffersTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    Offers *offer = [dataSource objectAtIndex: indexPath.row];
    cell.indexPath=indexPath;
    
    [cell updateCellWithData: offer];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tblView deselectRowAtIndexPath:indexPath animated:YES];
    BrandRedemViewController *brandVc=[self.storyboard instantiateViewControllerWithIdentifier:@"BrandRedemScene"];
    brandVc.offer = [dataSource objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:brandVc animated:YES];
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Web service Methods
- (void)getOffers
{
//    [Utils startActivityIndicatorInView:self.view withMessage:nil];
    [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
    NSMutableDictionary *dict = [Utils setPredefindValueForWebservice];
//    [dict setObject:@"6951" forKey:kAaramshopId];
    [dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:kStore_id] forKey:kAaramshopId];

    [self performSelector:@selector(callWebServiceToGetOffers:) withObject:dict afterDelay:0.1];
}
- (void)callWebServiceToGetOffers:(NSMutableDictionary *)aDict
{
    if (![Utils isInternetAvailable])
    {
//        [Utils stopActivityIndicatorInView:self.view];
        [AppManager stopStatusbarActivityIndicator];
        [Utils showAlertView:kAlertTitle message:kAlertCheckInternetConnection delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return;
    }
    [aaramShop_ConnectionManager getDataForFunction:kCurrentOfferURL withInput:aDict withCurrentTask:TASK_CURRENT_OFFERS andDelegate:self ];
}
- (void)responseReceived:(id)responseObject
{
//    [Utils stopActivityIndicatorInView:self.view];
    [AppManager stopStatusbarActivityIndicator];
    if(aaramShop_ConnectionManager.currentTask == TASK_CURRENT_OFFERS)
    {
        if([[responseObject objectForKey:kstatus] intValue] == 1)
        {
            [self parseData:[responseObject objectForKey:kOffers]];
        }
    }
}
- (void)didFailWithError:(NSError *)error
{
//    [Utils stopActivityIndicatorInView:self.view];
    [AppManager stopStatusbarActivityIndicator];
    [aaramShop_ConnectionManager failureBlockCalled:error];
}

#pragma mark - Parsing Data
- (void)parseData:(id)data
{
    if (!dataSource) {
        dataSource = [[NSMutableArray alloc] init];
    }
    [dataSource removeAllObjects];
    if([data count]>0)
    {
        for(int i =0 ; i < [data count] ; i++)
        {
            NSDictionary *dict = [data objectAtIndex:i];
            Offers *offer = [[Offers alloc] init];
            offer.activity_id = [NSString stringWithFormat:@"%@",[dict objectForKey:kActivity_id]];
            offer.activity_name =[NSString stringWithFormat:@"%@", [dict objectForKey:kActivity_name]];
            offer.company_name = [NSString stringWithFormat:@"%@",[dict objectForKey:kCompany_name]];
            offer.brand_name = [NSString stringWithFormat:@"%@",[dict objectForKey:kBrand_name]];
            offer.activity_details = [NSString stringWithFormat:@"%@",[dict objectForKey:kActivity_details]];
            offer.image = [NSString stringWithFormat:@"%@",[dict objectForKey:kBrandImage]];
            offer.start_date = [NSString stringWithFormat:@"%@",[dict objectForKey:kStart_date]];
            offer.end_date= [NSString stringWithFormat:@"Valid till %@",[dict objectForKey:kEnd_date]];
            offer.redemption = [NSString stringWithFormat:@"Redemption %@",[dict objectForKey:kRedemption]];
            [dataSource addObject:offer];
        }
        [tblView reloadData];
    }
}
@end
