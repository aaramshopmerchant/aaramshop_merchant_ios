//
//  OffersTableCell.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 26/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersTableCell : UITableViewCell
{
    UIImageView *imgBrand;
    UILabel *lblBrandName;
    UILabel *lblActivityName;
    UILabel *lblActivityDetail;
    UILabel *lblValidTill;
    UIButton *lblRedemption;
    UIView *subView;
}
@property (nonatomic, strong) NSIndexPath *indexPath;
-(void)updateCellWithData:(Offers  *)offer;
@end
