//
//  Offers.h
//  AaramShop_Merchant
//
//  Created by Neha Saxena on 27/05/2015.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Offers : NSObject

@property (nonatomic, strong) NSString *activity_id;
@property (nonatomic, strong) NSString *activity_name;
@property (nonatomic, strong) NSString *company_name;
@property (nonatomic, strong) NSString *brand_name;
@property (nonatomic, strong) NSString *activity_details;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *start_date;
@property (nonatomic, strong) NSString *end_date;
@property (nonatomic, strong) NSString *redemption;
@property (nonatomic, strong) NSString *validTill;
@end
