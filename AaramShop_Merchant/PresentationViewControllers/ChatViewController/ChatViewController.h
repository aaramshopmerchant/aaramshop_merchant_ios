//
//  ChatViewController.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tViewChat;

@end
