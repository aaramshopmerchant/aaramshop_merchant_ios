//
//  BrandRedemViewController.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 27/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "BrandRedemViewController.h"
#import "CustomAlertViewController.h"

@interface BrandRedemViewController ()
{
    NSMutableArray *dataSource;
    CustomAlertViewController *customAlertVC;
    AaramShop_ConnectionManager *aaramShop_ConnectionManager;
    AppDelegate *deleg;
    BOOL isValid;
}
@end

@implementation BrandRedemViewController
@synthesize scrollViewBrand;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     deleg = APP_DELEGATE;
    [self setUpNavigationBar];
    scrollViewBrand=[[AKKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, scrollViewBrand.frame.size.width, 0.01f)];
    scrollViewBrand.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    
    self.brandName.text = self.offer.activity_name;
    self.activityName.text = self.offer.activity_details;
    self.lblValidTill.text = self.offer.end_date;
    [self.imgViewBrand setImageWithURL:[NSURL URLWithString:self.offer.image] placeholderImage:[UIImage imageNamed:@"newRedemptionPepsiLogo"]];
    aaramShop_ConnectionManager = [[AaramShop_ConnectionManager alloc] init];
    aaramShop_ConnectionManager.delegate = self;

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpNavigationBar
{
    
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 190, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(0,0, 190, 44);
    UILabel* titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont fontWithName:@"Roboto_Regular" size:15];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.text = @"Brand Redemption Scheme";
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    UIImage *imgBack = [UIImage imageNamed:@"backBtn"];
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.bounds = CGRectMake( -10, 0, 30, 30);
    
    [backBtn setImage:imgBack forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barBtnBack = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    
    
    NSArray *arrBtnsLeft = [[NSArray alloc]initWithObjects:barBtnBack, nil];
    self.navigationItem.leftBarButtonItems = arrBtnsLeft;
    
    
    UIViewController *vCon ;
    
    [self.view addSubview: vCon.view];
}

-(void)backBtn
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - button clicked methods

- (IBAction)btnSubmit:(id)sender {
    [sender setEnabled:NO];
//    [txtEnterCoupon resignFirstResponder];
//    [txtMobile resignFirstResponder];
//    [txtMobile resignFirstResponder];
    if([self isValid])
    {
        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        NSMutableDictionary *dict = [Utils setPredefindValueForWebservice];

        [dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:kStore_id] forKey:kAaramshopId];
        [dict setObject:self.offer.activity_id forKey:kActivityId];
        [dict setObject:txtEnterCoupon.text forKey:kCoupon_code];
        if([txtMobile.text length]>9)
        {
            [dict setObject:txtMobile.text forKey:kMobile];
        }
        if([txtName.text length]>0)
        {
            [dict setObject:txtName.text forKey:kName];
        }
        [self callWebServiceToCheckCouponValidation:dict];
    }
    else
    {
        [sender setEnabled:YES];
    }
}
- (BOOL)isValid
{
    if([txtEnterCoupon.text length]==0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter coupon code." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    return YES;
}
- (void)enableInteraction:(BOOL)enable
{
    txtName.userInteractionEnabled = enable;
    txtMobile.userInteractionEnabled = enable;
    txtEnterCoupon.userInteractionEnabled = enable;
    submitBtn.userInteractionEnabled = enable;
}
#pragma mark - Web service methods

- (void)callWebServiceToCheckCouponValidation:(NSMutableDictionary *)aDict
{
    if (![Utils isInternetAvailable])
    {
        //        [Utils stopActivityIndicatorInView:self.view];
        [AppManager stopStatusbarActivityIndicator];
        [Utils showAlertView:kAlertTitle message:kAlertCheckInternetConnection delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        [submitBtn setEnabled:YES];
        return;
    }
    [aaramShop_ConnectionManager getDataForFunction:kVerifyCouponCodeURL withInput:aDict withCurrentTask:TASK_VERIFY_COUPON_CODE andDelegate:self ];
}
- (void)responseReceived:(id)responseObject
{
    //    [Utils stopActivityIndicatorInView:self.view];
    [submitBtn setEnabled:YES];
    [AppManager stopStatusbarActivityIndicator];
    if(aaramShop_ConnectionManager.currentTask == TASK_VERIFY_COUPON_CODE)
    {
        if([[responseObject objectForKey:kstatus] intValue] == 1)
        {
            customAlertVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"CustomAlertScreen"];
            customAlertVC.message =[responseObject objectForKey:kMessage];
            isValid = [[responseObject objectForKey:kIsValid] boolValue];
            customAlertVC.isValid = [[responseObject objectForKey:kIsValid] boolValue];         
            CGRect customAlertViewRect = self.view.bounds;
            customAlertVC.delegate=self;
            customAlertVC.view.frame = customAlertViewRect;
            
            [deleg.window addSubview:customAlertVC.view];
            [self enableInteraction:NO];

        }
        else
        {
            customAlertVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"CustomAlertScreen"];
            customAlertVC.message =[responseObject objectForKey:kMessage];
            isValid = NO;
            customAlertVC.isValid = NO;
            CGRect customAlertViewRect = self.view.bounds;
            customAlertVC.delegate=self;
            customAlertVC.view.frame = customAlertViewRect;
            
            [deleg.window addSubview:customAlertVC.view];
            [self enableInteraction:NO];

         }
    }
}
- (void)didFailWithError:(NSError *)error
{
    //    [Utils stopActivityIndicatorInView:self.view];
    [submitBtn setEnabled:YES];
    [AppManager stopStatusbarActivityIndicator];
    [aaramShop_ConnectionManager failureBlockCalled:error];
}
#pragma mark - UITextfield Delegates
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
        
        
    } else {
        // Not found, so remove keyboard.
//         [scrollViewBrand setContentOffset:CGPointMake(0, 0) animated:YES];
        [textField resignFirstResponder];
        [self btnSubmit:submitBtn];
        
    }
    return YES; // We do not want UITextField to insert line-breaks.
}
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    [scrollViewBrand setContentOffset:CGPointMake(0, 200) animated:YES];
//}
//-(void)hideKeyboard
//{
//    [scrollViewBrand setContentOffset:CGPointMake(0, 0) animated:YES];
//    [txtName resignFirstResponder];
//    [txtMobile resignFirstResponder];
//    [txtEnterCoupon resignFirstResponder];
//}
-(void)enableSubmitBtn
{
    [self enableInteraction:YES];
    txtEnterCoupon.text = @"";
    if(isValid)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
