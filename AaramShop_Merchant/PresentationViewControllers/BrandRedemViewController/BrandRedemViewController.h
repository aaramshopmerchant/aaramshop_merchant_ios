//
//  BrandRedemViewController.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 27/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertViewController.h"
@interface BrandRedemViewController : UIViewController<UIGestureRecognizerDelegate,AaramShop_ConnectionManager_Delegate,CustomAlertViewControllerDelegate>
{
    

    __weak IBOutlet UIButton *submitBtn;
    __weak IBOutlet PWTextField *txtName;
    __weak IBOutlet PWTextField *txtMobile;
    __weak IBOutlet PWTextField *txtEnterCoupon;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBrand;
@property (strong, nonatomic) IBOutlet AKKeyboardAvoidingScrollView *scrollViewBrand;
@property (strong, nonatomic) IBOutlet UILabel *brandName;
@property (strong, nonatomic) IBOutlet UILabel *activityName;
@property (strong, nonatomic) IBOutlet UILabel *lblValidTill;
- (IBAction)btnSubmit:(id)sender;
@property (nonatomic, retain) Offers *offer;
@end
