//
//  ForgotPasswordViewController.h
//  AaramShop
//
//  Created by Approutes on 30/04/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController<UIGestureRecognizerDelegate>
{
    
    __weak IBOutlet UIButton *sendBtn;
    __weak IBOutlet UIScrollView *scrollViewForgotPassword;
    __weak IBOutlet UITextField *txtfEmail;
}
- (IBAction)btnBackClick:(UIButton *)sender;
- (IBAction)btnSend:(id)sender;

@end
