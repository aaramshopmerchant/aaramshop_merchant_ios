//
//  SignupSecViewController.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupSecViewController : UIViewController<UIGestureRecognizerDelegate,AaramShop_ConnectionManager_Delegate>
{
    __weak IBOutlet PWTextField *txtState;
    __weak IBOutlet PWTextField *txtCity;
    __weak IBOutlet PWTextField *txtLocality;
    __weak IBOutlet PWTextField *txtPinCode;
    __weak IBOutlet PWTextField *txtAddress;
    __weak IBOutlet UIImageView *imgBackground;
    __weak IBOutlet PWTextField *txtSuggestedStoreCode;
    UIImage * effectImage;
    __weak IBOutlet UIButton *continueBtn;
    __weak IBOutlet MKMapView *mapViewLocation;
}
- (IBAction)btnContinue:(id)sender;
@end
