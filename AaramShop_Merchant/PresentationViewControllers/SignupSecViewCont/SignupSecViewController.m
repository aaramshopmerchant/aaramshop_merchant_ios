//
//  SignupSecViewController.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "SignupSecViewController.h"
#import "SignupThirdViewController.h"
#import "DDAnnotationView.h"

@interface SignupSecViewController ()
{
    NSData *imageData;
    CustomMapAnnotationView *viewOfCustomAnnotation;
    AppDelegate *appDeleg;
    NSString *strYourCurrentAddress;
    AaramShop_ConnectionManager *aaramShop_ConnectionManager;
}
@end

@implementation SignupSecViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self setUpNavigationBar];
    appDeleg = APP_DELEGATE;
    
    aaramShop_ConnectionManager = [[AaramShop_ConnectionManager alloc]init];
    aaramShop_ConnectionManager.delegate = self;

    if([[[NSUserDefaults standardUserDefaults]valueForKey:kStore_code]length]>0)
    {
        txtSuggestedStoreCode.text = [[NSUserDefaults standardUserDefaults] valueForKey:kStore_code];
    }
    imageData = [[NSUserDefaults standardUserDefaults] objectForKey:kImage];
    if (imageData) {
        UIImage* image = [UIImage imageWithData:imageData];
        effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:image];
        imgBackground.image = effectImage;
        
    }
    else
    {
        imgBackground.image = [UIImage imageNamed:@"bGroundThree.jpg"];
    }
    UITapGestureRecognizer *gst = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyboard)];
    gst.cancelsTouchesInView = NO;
    gst.delegate = self;
    [self.view addGestureRecognizer:gst];
    [self updateMapScreenFromLatitude:appDeleg.myCurrentLocation.coordinate.latitude andLongitude:appDeleg.myCurrentLocation.coordinate.longitude];
}
-(void)updateMapScreenFromLatitude:(CLLocationDegrees)Latitude andLongitude:(CLLocationDegrees)LongitudeValue
{
    CLLocationCoordinate2D coordinateValue = CLLocationCoordinate2DMake(Latitude, LongitudeValue);
    Annotation *annotation = [[Annotation alloc]initWithName:@"You" Address:strYourCurrentAddress Coordinate:coordinateValue imageUrl:@"" showMyLocation:YES];
    [mapViewLocation addAnnotation:annotation];
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for (id <MKAnnotation> annotation in mapViewLocation.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    
    region.center.latitude=(topLeftCoord.latitude+bottomRightCoord.latitude)/2;
    region.center.longitude=(bottomRightCoord.longitude+ topLeftCoord.longitude)/2;
    float LatDelta=(topLeftCoord.latitude-bottomRightCoord.latitude)*1.5;
    
    float LonDelta=(bottomRightCoord.longitude-topLeftCoord.longitude)*1.5;
    
    if (LatDelta>180)
        LatDelta=180;
    
    if (LonDelta>180)
        LonDelta=180;
    
    region.span.latitudeDelta = LatDelta;
    region.span.longitudeDelta = LonDelta;
    
    if(region.center.longitude == 0.00000000 && region.center.latitude==0)
        NSLog(@"Invalid region!");
    else
        [mapViewLocation setRegion:region animated:YES];
    
}

-(void)setUpNavigationBar
{
    CGRect headerTitleSubtitleFrame = CGRectMake(0, 0, 150, 44);
    UIView* _headerTitleSubtitleView = [[UILabel alloc] initWithFrame:headerTitleSubtitleFrame];
    _headerTitleSubtitleView.backgroundColor = [UIColor clearColor];
    _headerTitleSubtitleView.autoresizesSubviews = NO;
    
    CGRect titleFrame = CGRectMake(0,0, 150, 44);
    UILabel* titleView = [[UILabel alloc] initWithFrame:titleFrame];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont fontWithName:@"Roboto_Regular" size:15];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.text = @"Additional Details";
    titleView.adjustsFontSizeToFitWidth = YES;
    [_headerTitleSubtitleView addSubview:titleView];
    self.navigationItem.titleView = _headerTitleSubtitleView;
    
    UIImage *imgBack = [UIImage imageNamed:@"backBtn"];
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.bounds = CGRectMake( -10, 0, 30, 30);
    
    [backBtn setImage:imgBack forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtn) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barBtnBack = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    
    NSArray *arrBtnsLeft = [[NSArray alloc]initWithObjects:barBtnBack, nil];
    self.navigationItem.leftBarButtonItems = arrBtnsLeft;
    
    
    UIViewController *vCon ;
    
    [self.view addSubview: vCon.view];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(coordinateChanged_:) name:@"DDAnnotationCoordinateDidChangeNotification" object:nil];
//    [self createDataToGetAaramShops];
    [self getAddressFromLatitude:appDeleg.myCurrentLocation.coordinate.latitude andLongitude:appDeleg.myCurrentLocation.coordinate.longitude];
}
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DDAnnotationCoordinateDidChangeNotification" object:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)backBtn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnContinue:(id)sender
{
    [continueBtn setEnabled:NO];
    if([self isValid])
    {
        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        NSMutableDictionary *dict = [Utils setPredefindValueForWebservice];
        [dict setObject:txtSuggestedStoreCode.text forKey:kStoreCode];
        [dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:kStore_id] forKey:kStore_id];
        [dict setObject:txtAddress.text forKey:kAddress];
        [dict setObject:txtLocality.text forKey:kLocality];
        [dict setObject:txtPinCode.text forKey:kPincode];
        [dict setObject:txtCity.text forKey:kCity];
        [dict setObject:txtState.text forKey:kState];
        [self callWebServiceToSaveLocationDetails:dict];
    }
    else
    {
        [continueBtn setEnabled:YES];
    }
//    SignupThirdViewController *signUpThirdViewC = (SignupThirdViewController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignupThirdViewScene"];
//    [self.navigationController pushViewController:signUpThirdViewC animated:YES];
//    [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessfulNotificationName object:self userInfo:nil];
}
#pragma mark - keyboard Methods
-(void)hideKeyboard
{
//    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//    [txtEmail resignFirstResponder];
//    [txtFullName resignFirstResponder];
//    [txtPassword resignFirstResponder];
//    [txtStoreName resignFirstResponder];
}
#pragma mark - Get Address From Map
-(void)getAddressFromLatitude:(CLLocationDegrees)Latitude andLongitude:(CLLocationDegrees)LongitudeValue
{
    if ([Utils isInternetAvailable]) {

        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        NSString*urlStrings = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%.8f,%.8f&sensor=false",Latitude, LongitudeValue];
        
        NSURL *url=[NSURL URLWithString:[urlStrings stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc]init];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:queue
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                                   if (error) {
                                       [AppManager stopStatusbarActivityIndicator];
                                       NSLog(@"error:%@", error.localizedDescription);
                                   }
                                   else
                                   {
                                       data = [NSData dataWithContentsOfURL:url];
                                       NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                       NSLog(@"json=%@",json);
                                       NSString *str=[json objectForKey:@"status"];
                                       if ([str isEqualToString:@"OK"])
                                       {
                                           [self fetchedData:data];
                                       }
                                       else
                                           [AppManager stopStatusbarActivityIndicator];

                                       
                                       
                                   }
                                   
                               }];
    }

}
- (void)fetchedData:(NSData *)responseData
{
    @try
    {
        if(responseData.length>0)
        {
           // NSLog(@"responseData-----::::;;;;;;;;;%@",responseData);
            
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
            
            NSMutableArray *returnJsonValue=[json valueForKey:@"results"];
            NSLog(@"%@",returnJsonValue);
            
                if(returnJsonValue.count>0)
                {
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{

                        NSArray *arr=[returnJsonValue objectAtIndex:0];
                        NSString *stringTaggedLocationloc=[arr valueForKey:@"formatted_address"];
                        NSArray * arr1 = [arr valueForKey:@"address_components"];
                        NSArray *items = [stringTaggedLocationloc componentsSeparatedByString:@","];
                        NSDictionary *dic3 = [arr1 objectAtIndex:3];
                        txtLocality.text = [dic3 objectForKey:@"long_name"];
                        NSDictionary *dic4 = [arr1 objectAtIndex:4];
                        txtCity.text = [dic4 objectForKey:@"long_name"];
                        NSDictionary *dict5 = [arr1 objectAtIndex:5];
                        txtState.text = [dict5 objectForKey:@"long_name"];
                        
                        NSDictionary *dict7 = [arr1 objectAtIndex:7];

                        txtPinCode.text = [dict7 objectForKey:@"long_name"];

                    }];
                }
            
//            if(returnJsonValue.count>0)
//            {
//                NSArray *arr=[returnJsonValue objectAtIndex:0];
//                NSString *stringTaggedLocationloc=[arr valueForKey:@"formatted_address"];
//        //        NSArray * arr1 = [arr valueForKey:@"address_components"];
//                NSArray *items = [stringTaggedLocationloc componentsSeparatedByString:@","];
//                
//                txtLocality.text = [items objectAtIndex:2];
//                txtCity.text = [items objectAtIndex:3];
//                txtState.text = [items objectAtIndex:4];
//            }
            
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@",exception);
    }
    [AppManager stopStatusbarActivityIndicator];

}
#pragma mark -
#pragma mark DDAnnotationCoordinateDidChangeNotification

- (void)coordinateChanged_:(NSNotification *)notification {
    
//    Annotation *annotation = notification.object;
}

#pragma mark -
#pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    
    if (oldState == MKAnnotationViewDragStateDragging) {
        for (Annotation *annotation in mapViewLocation.annotations)
        {
            if ([annotation isKindOfClass:[Annotation class]]) {
                Annotation *obj = (Annotation *)annotation;
                if (obj.isMyLocation) {
                    [self getAddressFromLatitude:annotation.coordinate.latitude andLongitude:annotation.coordinate.longitude];
                    annotation.Address = strYourCurrentAddress;
                }
            }
        }
        
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    static NSString * const kPinAnnotationIdentifier = @"PinIdentifier";
    MKAnnotationView *draggablePinView = [mapViewLocation dequeueReusableAnnotationViewWithIdentifier:kPinAnnotationIdentifier];
    
    if ([annotation isKindOfClass:[Annotation class]]) {
        Annotation *objAnnotation = (Annotation *)annotation;
        if (objAnnotation.isMyLocation) {
            if (draggablePinView) {
                draggablePinView.annotation = annotation;
            }
            else
            {
                draggablePinView = [DDAnnotationView annotationViewWithAnnotation:annotation reuseIdentifier:kPinAnnotationIdentifier mapView:mapViewLocation];
                draggablePinView.draggable = YES;
                
                if ([draggablePinView isKindOfClass:[DDAnnotationView class]]) {
                    // draggablePinView is DDAnnotationView on iOS 3.
                } else {
                    draggablePinView.draggable = YES;
                    // draggablePinView instance will be built-in draggable MKPinAnnotationView when running on iOS 4.
                }
            }
            
        }
        else
        {
            draggablePinView.enabled = YES;
            draggablePinView.canShowCallout = NO;
            draggablePinView.calloutOffset = CGPointMake(0, 0);
            draggablePinView.image = [UIImage imageNamed:@"mapPin.png"];
            draggablePinView.annotation = annotation;
            return draggablePinView;
            
        }
    }
    
    return draggablePinView;
}

#pragma mark-

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if(![view.annotation isKindOfClass:[MKUserLocation class]])
    {
        if ([view.annotation isKindOfClass:[Annotation class]]) {
            Annotation *objAnnotation = (Annotation *)view.annotation;
            if (objAnnotation.isMyLocation) {
                viewOfCustomAnnotation = (CustomMapAnnotationView *)[[[NSBundle mainBundle] loadNibNamed:@"CustomMapAnnotationView" owner:self options:nil]objectAtIndex:0];
                viewOfCustomAnnotation.backgroundColor = [UIColor clearColor];
                CGRect calloutViewFrame = viewOfCustomAnnotation.frame;
                calloutViewFrame.origin = CGPointMake(-calloutViewFrame.size.width/2 + 7, -calloutViewFrame.size.height);
                
                viewOfCustomAnnotation.frame = calloutViewFrame;
                
                [viewOfCustomAnnotation.lblName setText:[(Annotation*)[view annotation] Name]];
                
                [viewOfCustomAnnotation.lblAddress setText:[(Annotation*)[view annotation] Address]];
                [viewOfCustomAnnotation.lblAddress setNumberOfLines:2];
                
                viewOfCustomAnnotation.userInteractionEnabled=YES;
                [view addSubview:viewOfCustomAnnotation];
                
            }
        }
    }
}



-(BOOL)prefersStatusBarHidden
{
    return YES;
}
#pragma mark - UITextfield Delegates
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
        
        
    } else {
        // Not found, so remove keyboard.
        
        [textField resignFirstResponder];
        [self btnContinue:continueBtn];
        
    }
    return YES; // We do not want UITextField to insert line-breaks.
}

#pragma mark - Calling Web service
- (BOOL)isValid
{
    txtSuggestedStoreCode.text = [txtSuggestedStoreCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(txtAddress.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter Address." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtCity.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter City." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtPinCode.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter Pincode." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtState.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter State." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtLocality.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter Locality." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtSuggestedStoreCode.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter Store code." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtSuggestedStoreCode.text.length < 4)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter Store code." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    return YES;
}
- (void)callWebServiceToSaveLocationDetails:(NSMutableDictionary*)aDict
{
    if (![Utils isInternetAvailable])
    {
        [continueBtn setEnabled:YES];
        [AppManager stopStatusbarActivityIndicator];
        [Utils showAlertView:kAlertTitle message:kAlertCheckInternetConnection delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return;
    }
    
    [aaramShop_ConnectionManager getDataForFunction:kMerchantAddressURL withInput:aDict withCurrentTask:TASK_MERCHANT_ADDRESS Delegate:self andMultipartData:nil];
}
-(void) didFailWithError:(NSError *)error
{
    [continueBtn setEnabled:YES];
    [aaramShop_ConnectionManager failureBlockCalled:error];
}
-(void) responseReceived:(id)responseObject
{
    [continueBtn setEnabled:YES];
    
    if (aaramShop_ConnectionManager.currentTask == TASK_MERCHANT_ADDRESS) {
        if([[responseObject objectForKey:kstatus]intValue]==1)
        {
            [AppManager saveDataToNSUserDefaults:responseObject];
            SignupThirdViewController *signupThirdVwController = (SignupThirdViewController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignupThirdViewController"];
            [self.navigationController pushViewController:signupThirdVwController animated:YES];
            //            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessfulNotificationName object:self userInfo:nil];
        }
        else
        {
            [Utils showAlertView:kAlertTitle message:[responseObject objectForKey:kMessage] delegate:self cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        }
        
    }
}

@end
