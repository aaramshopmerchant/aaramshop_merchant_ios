//
//  SignupViewController.h
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKKeyboardAvoidingScrollView.h"
@interface SignupViewController : UIViewController<UINavigationControllerDelegate
,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,AaramShop_ConnectionManager_Delegate>
{
    
    __weak IBOutlet UIImageView *imgBackground;
    __weak IBOutlet UIImageView *imgVUser;
    UIImage * effectImage;
    __weak IBOutlet UIButton *continueBtn;
    


    __weak IBOutlet UITextField *txtFullName;
    
    __weak IBOutlet UITextField *txtStoreName;
    
    __weak IBOutlet UITextField *txtEmail;
    
    __weak IBOutlet UITextField *txtPassword;
    UIImage *imgUser;
}
@property (strong, nonatomic) IBOutlet AKKeyboardAvoidingScrollView *scrollView;
- (IBAction)btnPickImage:(id)sender;
- (IBAction)btnBackClick:(id)sender;
- (IBAction)btnContinue:(id)sender;
@end
