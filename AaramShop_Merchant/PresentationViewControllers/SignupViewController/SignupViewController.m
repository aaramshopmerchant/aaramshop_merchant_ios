//
//  SignupViewController.m
//  AaramShop_Merchant
//
//  Created by Arbab Khan on 25/05/15.
//  Copyright (c) 2015 Arbab. All rights reserved.
//

#import "SignupViewController.h"
#import "SignupSecViewController.h"

@interface SignupViewController ()
{
    NSData* imageData;
    AaramShop_ConnectionManager *aaramShop_ConnectionManager;
}
@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    aaramShop_ConnectionManager = [[AaramShop_ConnectionManager alloc]init];
    aaramShop_ConnectionManager.delegate = self;
    // Scrollview setup

    
    [self.scrollView contentSizeToFit];
    
    self.scrollView=[[AKKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, 0.01f)];
    
    _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    [_scrollView setContentSize:CGSizeMake(320, [UIScreen mainScreen].bounds.size.height+200)];
        imgVUser.layer.cornerRadius = imgVUser.frame.size.height/2;
    imgVUser.clipsToBounds = YES;
    
    imageData = [[NSUserDefaults standardUserDefaults] objectForKey:kImage];
    if (imageData) {
        UIImage* image = [UIImage imageWithData:imageData];
        effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:image];
        imgBackground.image = effectImage;
        imgVUser.image = image;
        
    }
    else
    {
        imgBackground.image = [UIImage imageNamed:@"bGroundThree.jpg"];
        imgVUser.image = [UIImage imageNamed:@"enterMobileNoDefaultCameraIcon"];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnContinue:(id)sender {
    [continueBtn setEnabled:NO];
    if([self isValid])
    {
        [AppManager startStatusbarActivityIndicatorWithUserInterfaceInteractionEnabled:YES];
        NSMutableDictionary *dict = [Utils setPredefindValueForWebservice];
        [dict setObject:txtEmail.text forKey:kEmailId];
        [dict setObject:txtFullName.text forKey:kFullName];
        [dict setObject:txtStoreName.text forKey:kStoreName];
        [dict setObject:txtPassword.text forKey:kpassword];
        [dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:kStore_id] forKey:kStore_id];
        [self callWebServiceToSaveMerchantDetails:dict];
    }
    else
    {
        [continueBtn setEnabled:YES];
    }
 
//    SignupSecViewController *signUpSecViewC = (SignupSecViewController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignupSecViewScene"];
//    [self.navigationController pushViewController:signUpSecViewC animated:YES];
}

#pragma mark - Calling Web service
- (BOOL)isValid
{
    txtPassword.text = [txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    if(txtFullName.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter full name." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtStoreName.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter store name." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtEmail.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter email." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    if(txtPassword.text.length == 0)
    {
        [Utils showAlertView:kAlertTitle message:@"Please enter password." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return NO;
    }
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL b = [emailTest evaluateWithObject:txtEmail.text];
    if (b == NO)
    {
        [Utils showAlertView:kAlertTitle message:@"Email is not in valid format" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
    if(txtPassword.text.length<6)
    {
        [Utils showAlertView:kAlertTitle message:@"Password is too short.\n (Min. 6 chars.)" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }

    return YES;
}
- (void)callWebServiceToSaveMerchantDetails:(NSMutableDictionary*)aDict
{
    NSLog(@"***** data Merchant details: %@",aDict);
    if (![Utils isInternetAvailable])
    {
        [continueBtn setEnabled:YES];
        [AppManager stopStatusbarActivityIndicator];
        [Utils showAlertView:kAlertTitle message:kAlertCheckInternetConnection delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        return;
    }
    
    [aaramShop_ConnectionManager getDataForFunction:kMerhantDetailsURL withInput:aDict withCurrentTask:TASK_MERCHANT_DETAIL Delegate:self andMultipartData:nil];
}
-(void) didFailWithError:(NSError *)error
{
    [continueBtn setEnabled:YES];
    [aaramShop_ConnectionManager failureBlockCalled:error];
}
-(void) responseReceived:(id)responseObject
{
    [continueBtn setEnabled:YES];

    if (aaramShop_ConnectionManager.currentTask == TASK_MERCHANT_DETAIL) {
        if([[responseObject objectForKey:kstatus]intValue]==1)
        {
            [AppManager saveDataToNSUserDefaults:responseObject];
//            SignupSecViewController *signupSecVwController = (SignupSecViewController*)[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignupSecViewController"];
//            [self.navigationController pushViewController:signupSecVwController animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLoginSuccessfulNotificationName object:self userInfo:nil];
        }
        else
        {
            [Utils showAlertView:kAlertTitle message:[responseObject objectForKey:kMessage] delegate:self cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
        }
        
    }
}

- (IBAction)btnPickImage:(id)sender {
    NSMutableArray * arrbuttonTitles = [[NSMutableArray alloc]initWithObjects:@"Camera",@"Select from Library", nil];
    
    [arrbuttonTitles addObject:@"Cancel"];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil delegate: self cancelButtonTitle: nil destructiveButtonTitle: nil otherButtonTitles: nil];
    
    for (NSString *title in arrbuttonTitles) {
        [actionSheet addButtonWithTitle: title];
    }
    [actionSheet setCancelButtonIndex: [arrbuttonTitles count] - 1];
    
    [actionSheet showInView:self.view];
}

- (IBAction)btnBackClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0:
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.allowsEditing=YES;
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePicker animated:YES completion:^{}];
            }
            else
            {
                [Utils showAlertView:@"" message:@"Camera is not available." delegate:nil cancelButtonTitle:kAlertBtnOK otherButtonTitles:nil];
            }
        }
            break;
        case 1:
        {
            UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.allowsEditing=YES;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:YES completion:^{}];
        }
            break;
        case 2:
        {
            [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
        }
            break;
            
        default:
            // Do Nothing.........
            break;
    }
}


#pragma - mark Selecting Image from Camera and Library
- (void)imagePickerController:(UIImagePickerController *)pickerVw didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [pickerVw dismissViewControllerAnimated:YES completion:^{
        
        imgUser = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        //myImagePickerController.allowsEditing=YES;
        imageData = [NSMutableData dataWithData:UIImageJPEGRepresentation(imgUser, 1.0)];
        imgVUser.image = imgUser;
        imgBackground.image = imgUser;
        effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:imgUser];
        imgBackground.image=effectImage;
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(imgUser)            forKey:kImage];
        imgBackground.contentMode = UIViewContentModeScaleAspectFill;
    }];
    
}
#pragma mark - UITextfield Delegates
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
        
        
    } else {
        // Not found, so remove keyboard.
       
        [textField resignFirstResponder];
        [self btnContinue:continueBtn];
        
    }
    return YES; // We do not want UITextField to insert line-breaks.
}

@end
