//
//  FlagListTableViewController.h
//  GuessThis
//
//  Created by Madhup Singh Yadav on 4/15/14.
//  Copyright (c) 2014 AppUs.in LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlagListTableViewController : UITableViewController<UISearchBarDelegate, UISearchDisplayDelegate>

@end
