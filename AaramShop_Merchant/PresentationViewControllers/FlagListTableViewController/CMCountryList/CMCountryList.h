//
//  CMCountryList.h
//  AaramShop
//
//  Created by Pradeep Singh on 23/05/15.
//  Copyright (c) 2015 Approutes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMCountryList : NSObject
@property(nonatomic,strong) NSString *flagName;
@property(nonatomic,strong) NSString *phoneCode;
@property(nonatomic,strong) NSString *countryName;
@end
